#!/bin/sh

echo "This script will automaticaly install a "ready-to-work" arch linux install on your system"
echo "This script is only intended to work on a Acer aspire e5-774g"
echo
echo
echo "------ Phase -1: base arch install ------"
while [ "$BASIC" != "y" ] && [ "$BASIC" != "n" ]
do
	echo "Is basic arch already installed on your pc? (y/n)"
	read BASIC
done

if [ "$BASIC" = "n" ] ; then
	echo "Starting a new installation of arch linux"
	echo
	echo "-- Partitioning --"
	lsblk -f
	echo "Enter the dev identifier of your disk"
	read IDENTI
	sfdisk /dev/${IDENTI} < sda.sfdisk
	echo
	echo "-- Formating --"
	mkfs.ext4 /dev/${IDENTI}2
	mkfs.fat -F32 /dev/${IDENTI}1
	echo
	echo "-- Mounting --"
	mount /dev/${IDENTI}2 /mnt
	mkdir /mnt/efi
	mount /dev/${IDENTI}1 /mnt/efi
	echo
	echo "-- Base packages install --"
	pacstrap /mnt base linux linux-firmware vim networkmanager man-db man-pages texinfo git grub efibootmgr intel-ucode
	echo
	echo "-- Fstab --"
	genfstab -U /mnt >> /mnt/etc/fstab
	echo
	echo "-- Chrooting --"
	cp ~/arch-installation-script/arch-chroot-script.sh /mnt/
	arch-chroot /mnt /arch-chroot-script.sh
	echo
	echo "-- Cleaning up --"
	cp -r ~/arch-installation-script /mnt/root/
	rm /mnt/arch-chroot-script.sh
	umount -R /mnt
	echo
	echo "Arch basic installation finished"
	echo "You can know reboot your system"
	
else
	echo "------ Phase 0: Basic system administration ------"
	echo
	echo "Enter your name"
	read NAME
	useradd -m ${NAME}
	passwd ${NAME}
	gpasswd -a ${NAME} wheel
	sed -i 's/^# %wheel ALL=(ALL) ALL$/%wheel ALL=(ALL) ALL/' /etc/sudoers
	echo
	echo "For this moment, security is not part of this installer"
	echo
	
	echo "------ Phase 1: Package management ------"
	echo
	sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
	pacman --noconfirm -Suyyy
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -csi
	cd
	rm -r yay
	yay -Suy
	echo
	echo "------ Phase 2: Booting ------"
	yay -S mkinitcpio-numlock
	sed -i 's/<encrypt>/numlock &/' /etc/mkinitcpio.conf
	mkinitcpio -P
	echo
	echo "------ Phase 3: Graphical user interface ------"
	echo "Done"
fi

