#!/bin/sh

echo "You are know chrooted in your system"
echo
echo "-- Time zone --"
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc
echo
echo "-- Localization --"
sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
sed -i 's/#fr_FR.UTF-8/fr_FR.UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=fr-latin1" > /etc/vconsole.conf
echo
echo "-- Networking --"
echo "Arch-LP" > /etc/hostname
systemctl enable NetworkManager.service
echo
echo "-- Re-generating mkinitcpio --"
mkinitcpio -P
echo
echo "-- Root password --"
passwd
echo
echo "-- Boot loader --"
grub-install --target=x86_64-efi --efi-directory=efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
